package tn.esprit.kadem.entities;




import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Contrat implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idContrat;
    @Temporal(TemporalType.DATE)
    Date dateDebutC;
    Date dateFinC;
    @Enumerated(EnumType.STRING)
    Specialite specialite;
    Boolean archive;
    Float montantC;
    //synchronisation
    @ManyToOne(cascade = CascadeType.ALL)
    private Etudiant etudiant;


}

